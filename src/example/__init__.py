""" example """
import sys
from example.main import Main
from example.constants import __version__


def main():
    """ Main """
    Main().main()


if __name__ == '__main__':
    sys.exit(main())
